class Gurus{
    visitGurusPage(){
        cy.visit("https://app.ambition.guru/gurus")
    }
    verifyGuruName(){
        cy.get('h6').should('be.visible')
    }
    switchFirstCategory(){
        cy.get(':nth-child(2) > .custom-tab-item').click()
    }
    switchSecondCategory(){
        cy.get(':nth-child(3) > .custom-tab-item').click()
    }
    switchThirdCategory(){
        cy.get(':nth-child(3) > .custom-tab-item').click()
    }

    getSearchField(){
        return cy.get(':nth-child(3) > .q-field > .q-field__inner > .q-field__control')
    }

    searchGuru(guru){
        this.getSearchField().type(guru)
        this.getSearchField().type("{enter}")
    }

    verifyGuruSearch(guru){
        cy.get('h6').should('contain',guru)
    }

    clickAsk(){
        cy.get(':nth-child(4) > .d-flex > .user-info > .q-btn').click()
    }

    verifyAskPage(){
        cy.get('.conversation-footer > .q-form > .d-flex').should('be.visible')
    }

    verifyGuruPicture(){
        cy.get('.q-img__container img[src*="amrit-raj-aryal-3-medium.png"]').should('be.visible');

    }


}

export default Gurus