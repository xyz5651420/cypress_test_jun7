class Login{
    visitLoginPage(){
        cy.visit("https://app.ambition.guru/login")
    }

    getMobileInputField(){
        return cy.get('.q-field__control')
    }

    getLoginButton(){
        return cy.get('.q-btn')
    }

    enterMobileNumber(mobileNumber){
        this.getMobileInputField().type(mobileNumber)
    }

    clickLoginButton(){
        this.getLoginButton().click()
    }

    getOtpField(){
        return cy.get(':nth-child(2) > .q-field > .q-field__inner > .q-field__control')
    }

    inputOtp(otp){
        this.getOtpField().type(otp)
    }

    getVerifyOtp(){
        return cy.get('.q-btn')
    }

    clickVerifyOtp(){
        this.getVerifyOtp().click()
    }

    loginVerify(){
        cy.get('.d-flex > .q-item__section--main > .package-nav-overflow').should("be.visible")
    }

    verifyNotRegistered(){
        cy.get('.q-notification').should("be.visible")
    }

    verifyInvalidNumber(){
        cy.get('.q-field__messages > div').should('contain','Mobile Number must be in numbers')
    }

    verifyInvalidOtp(){
        cy.get('.tw-text-sm').should('contain','OTP token you have submitted is invalid.')
    }

    verifyEmptyOtp(){
        cy.get('.q-field__messages > div').should('contain','Otp code is required')
    }

    clearOtpField(){
        this.getOtpField().clear()
    }

    compareInputNumber(moreThanTenNumber){
        this.getMobileInputField().should('not.eq',moreThanTenNumber)
    }

    logOut(){
        cy.get('.user-icon').click()
        cy.get(':nth-child(9) > .q-avatar > .q-avatar__content').click()
    }
    



}

export default Login