class Resources{

    visitResourcesPage(){
        cy.visit("https://app.ambition.guru/resources")
    }

    verifyResourceList(){
        cy.get('h3').should('be.visible')
    }

    switchCategoryInformational(){
        cy.get('.swiper-slide-next > .custom-tab-item').click()
    }
    switchCategoryAmbitionGuru(){
        cy.get(':nth-child(3) > .custom-tab-item').click()
    }
    switchCategoryHelp(){
        cy.get(':nth-child(4) > .custom-tab-item').click()
    }
    switchTipsTricks(){
        cy.get(':nth-child(5) > .custom-tab-item').click()
    }
    verifyVideoNavigation(){
        cy.get(':nth-child(1) > .content > .ag-bg-white > .tw-pt-4').find('a').should('have.attr','href').and('include','youtube.com')
    }

    copyLink(){
        cy.get(':nth-child(5) > .content > .ag-bg-white > .tw-pt-4 > :nth-child(2) > .q-img > .q-img__container > .q-img__image').click()
    }

    verifyLinkCopy(){
        cy.get('.q-notification__message').should('contain','Link copied to clipboard')
    }

    verifyThumbnailVisibility(){
        cy.get('.q-img__container').should("be.visible")
    }

    verifyCategoryList(){
        cy.get('.custom-tab-item').should('exist');
    }

}

export default Resources