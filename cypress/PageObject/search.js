class Search{
    getSearchField(){
        return cy.get('.flex > .q-field > .q-field__inner > .q-field__control')
    }

    emptySearch(){
        this.getSearchField().type("{enter}")
    }

    noDataFoundVerify(){
        cy.get(':nth-child(3) > :nth-child(1) > .d-flex').should('contain','No data found')
    }

    searchTerm(term){
        this.getSearchField().type(term)
        this.getSearchField().type("{enter}")
    }

    searchVerify(){
        cy.get('.col-md-8').should('not.contain','No data found')
    }

    searchWithouEnter(term){
        this.getSearchField().type(term)
    }

    clickSearch(){
        cy.get('.q-field__control-container > .d-flex > .q-img > .q-img__container > .q-img__image').click()
    }

}

export default Search