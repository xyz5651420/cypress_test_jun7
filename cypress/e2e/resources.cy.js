import Login from "../PageObject/login"
import Resources from "../PageObject/resources"
describe("Resources",()=>{
    const loginPage = new Login()
    const resourcePage = new Resources()
    

    beforeEach(()=>{
        cy.viewport(1280, 720);
        loginPage.visitLoginPage()
        cy.contains("Login to your account.")
        cy.fixture('loginFixtures').then((loginUser)=>{
            const loginDetails = loginUser.userDetails[0]

            loginPage.enterMobileNumber(loginDetails.mobileNumber)
            loginPage.clickLoginButton()
            //code to wait for OTP field to show up else skip finding the OTP field
            cy.get('body').then(($body)=>{
                if($body.find(':nth-child(2) > .q-field > .q-field__inner > .q-field__control').length>0){
                    loginPage.inputOtp(loginDetails.otp)
                    loginPage.clickVerifyOtp()
                }
            })
        })
    })
    afterEach(()=>{
        cy.get('.user-icon').click()
        cy.get(':nth-child(9) > .q-avatar > .q-avatar__content').click()
        cy.contains("Login to your account.")
        cy.clearLocalStorage();
        cy.clearCookies();
    })

    it("Resource videos",()=>{
        resourcePage.visitResourcesPage()
        resourcePage.verifyResourceList()
    })

    it("Resource Video leads to youtube",()=>{
        resourcePage.visitResourcesPage()
        cy.contains("Resources")
        resourcePage.verifyVideoNavigation()
    })

    it("Video Link copy",()=>{
        resourcePage.visitResourcesPage()
        cy.contains("Resources")
        resourcePage.copyLink()
        cy.wait(6000)
        resourcePage.verifyLinkCopy()
        })
    it("category list",()=>{
        resourcePage.visitResourcesPage()
        cy.contains("Resources")
        resourcePage.verifyCategoryList()
        })
        
    it("Resources Ambitionguru Category",()=>{
        resourcePage.visitResourcesPage()
        cy.contains("Resources")
        resourcePage.switchCategoryAmbitionGuru()
        resourcePage.verifyResourceList()
        })
    
    it("Resources videos thumbnail",()=>{
        resourcePage.visitResourcesPage()
        cy.contains("Resources")
        resourcePage.verifyThumbnailVisibility()
        })
        
    // it("Resources Help Category",()=>{
    //         resourcePage.switchCategoryHelp()//no videos here
    //         cy.wait(2000)
    //         resourcePage.verifyResourceList()
    //         })
            
    //  it("Resources Informational category",()=>{
    //             resourcePage.switchCategoryInformational()//no resource videos here
    //             cy.wait(2000)
    //             resourcePage.verifyResourceList()
    //         })
    })