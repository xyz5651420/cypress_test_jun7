import Login from "../PageObject/login"
import Gurus from "../PageObject/gurus"

describe("Gurus page",()=>{
    const loginPage = new Login()
    const gurusPage = new Gurus()

    beforeEach(()=>{
        cy.viewport(1280, 720);
        loginPage.visitLoginPage()
        cy.contains("Login to your account.")
        cy.fixture('loginFixtures').then((loginUser)=>{
            const loginDetails = loginUser.userDetails[0]

            loginPage.enterMobileNumber(loginDetails.mobileNumber)
            loginPage.clickLoginButton()
            //code to wait for OTP field to show up else skip finding the OTP field
            cy.get('body').then(($body)=>{
                if($body.find(':nth-child(2) > .q-field > .q-field__inner > .q-field__control').length>0){
                    loginPage.inputOtp(loginDetails.otp)
                    loginPage.clickVerifyOtp()
                }
            })
        })
    })
    afterEach(()=>{
        cy.get('.user-icon').click()
        cy.get(':nth-child(9) > .q-avatar > .q-avatar__content').click()
        cy.contains("Login to your account.")
        cy.clearCookies();
        cy.clearLocalStorage();
    })

    it("Display Gurus names",()=>{
        cy.contains("Gurus")
        gurusPage.visitGurusPage()
        gurusPage.verifyGuruName()
    })

    it("Filter Gurus based on subject",()=>{
        cy.contains("Gurus")
        gurusPage.visitGurusPage()
        gurusPage.switchFirstCategory()
        gurusPage.verifyGuruName()
        gurusPage.switchSecondCategory()
        gurusPage.verifyGuruName()
        gurusPage.switchThirdCategory()
        gurusPage.verifyGuruName()
    })

    it("Search Guru",()=>{
        cy.contains("Gurus")
        gurusPage.visitGurusPage()
        cy.fixture("gurus").then((guru)=>{
            const guruName = guru.guruu
            gurusPage.searchGuru(guruName)
            gurusPage.verifyGuruSearch(guruName)
        })
    })

    it("Ask Gurus",()=>{
        cy.contains("Gurus")
        gurusPage.visitGurusPage()
            gurusPage.clickAsk()
            gurusPage.verifyAskPage()
    
    })

    it("Guru's pictures display",()=>{
        cy.contains("Gurus")
        gurusPage.visitGurusPage()
        gurusPage.verifyGuruPicture()
    })

    
})