import Login from "../PageObject/login"
import Search from "../PageObject/search"

describe("Search Bar",()=>{
    const loginPage = new Login()
    const searchPage = new Search()

    beforeEach(()=>{
        cy.viewport(1280, 720);
        loginPage.visitLoginPage()
        cy.fixture('loginFixtures').then((loginUser)=>{
            const loginDetails = loginUser.userDetails[0]

            loginPage.enterMobileNumber(loginDetails.mobileNumber)
            loginPage.clickLoginButton()
            cy.wait(2000)
            //code to wait for OTP field to show up else skip finding the OTP field
            cy.get('body').then(($body)=>{
                if($body.find(':nth-child(2) > .q-field > .q-field__inner > .q-field__control').length>0){
                    loginPage.inputOtp(loginDetails.otp)
                    loginPage.clickVerifyOtp()
                }
            })
        })
    })
    afterEach(()=>{
        cy.get('.user-icon').click()
        cy.get(':nth-child(9) > .q-avatar > .q-avatar__content').click()
        cy.clearLocalStorage();
        cy.clearCookies();
        cy.contains("Login to your account")
    })

    it("Search with single word with relevant search term",()=>{
        cy.fixture('searchFixtures').then((search)=>{
            cy.contains("Search")
            const searchQuery = search.relevantSearchTerm
            searchPage.searchTerm(searchQuery)
            searchPage.searchVerify()
        })
    })

    it("Search with empty search bar",()=>{
        cy.contains("Search")
        searchPage.emptySearch()
        searchPage.noDataFoundVerify()
     })

    it("Search with irrelevant search term",()=>{
        cy.fixture('searchFixtures').then((search)=>{
            cy.contains("Search")
            const searchQuery = search.irrelevantSearchTerm
            searchPage.searchTerm(searchQuery)
            searchPage.noDataFoundVerify()
        })
    })

    it("Search with multiple words with relevant search term",()=>{
            cy.fixture('searchFixtures').then((search)=>{
                cy.contains("Search")
                const searchQuery = search.relevantMultipleSearchTerm
                searchPage.searchTerm(searchQuery)
                searchPage.searchVerify()
            })
        })

    it("Search with multiple words with irrelevant search term",()=>{
            cy.fixture('searchFixtures').then((search)=>{
                cy.contains("Search")
                const searchQuery = search.irrelevantMultipleSearchTerm
                searchPage.searchTerm(searchQuery)
                searchPage.noDataFoundVerify()
            })
        })

    it("Search with uppercase search terms",()=>{
            cy.fixture('searchFixtures').then((search)=>{
                cy.contains("Search")
                const searchQuery = search.upperCaseTerm
                searchPage.searchTerm(searchQuery)
                searchPage.searchVerify()
                cy.wait(2000)
            })
        })
    
    it("Search with special characters in the search term",()=>{
        cy.fixture('searchFixtures').then((search)=>{
            cy.contains("Search")
            const searchQuery = search.specialSearchTerm
            searchPage.searchTerm(searchQuery)
            searchPage.searchVerify()
            cy.wait(2000)
        })
        })
    it("Search with Nepali language",()=>{
        cy.fixture('searchFixtures').then((search)=>{
            cy.contains("Search")
            const searchQuery = search.nepaliSearchTerm
            searchPage.searchTerm(searchQuery)
            searchPage.searchVerify()
            cy.wait(2000)
        })
    })
    it("Search by clicking search icon",()=>{
        cy.fixture('searchFixtures').then((search)=>{
            cy.contains("Search")
            const searchQuery = search.relevantSearchTerm
            searchPage.searchWithouEnter(searchQuery)
            searchPage.clickSearch() //clicking search button wont start the search operation 
            searchPage.searchVerify()
            cy.wait(2000)
        })
    })


    
    


    




})