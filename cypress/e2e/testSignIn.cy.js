import Login from "../PageObject/login"

describe("Login Tests",()=>{

    const loginPage = new Login()

    beforeEach(()=>{
        cy.viewport(1280, 720);
    })
    afterEach(()=>{
        cy.clearLocalStorage();
        cy.clearCookies();
        
    })

    it("Successful Login",()=>{
        loginPage.visitLoginPage()
        
        cy.fixture('loginFixtures').then((loginUser)=>{
            const loginDetails = loginUser.userDetails[0]

            loginPage.enterMobileNumber(loginDetails.mobileNumber)
            loginPage.clickLoginButton()

            //code to wait for OTP field to show up else skip finding the OTP field
            cy.get('body').then(($body)=>{
                if($body.find(':nth-child(2) > .q-field > .q-field__inner > .q-field__control').length>0){
                    loginPage.inputOtp(loginDetails.otp)
                    loginPage.clickVerifyOtp()
                }
            })
            loginPage.loginVerify()
            loginPage.logOut() 
        })
    })

    it("Login with unregistered mobile number",()=>{
        loginPage.visitLoginPage()
        cy.contains("Login to your account")
        
        cy.fixture('loginFixtures').then((cantloginUser)=>{
        const loginDetails = cantloginUser.invalidUserDetails[0]
        loginPage.enterMobileNumber(loginDetails.unregisteredUser)
        loginPage.clickLoginButton()
        loginPage.verifyNotRegistered()
        cy.contains("Register to your account.")
        })
    })

    it("Login with invalid mobile number",()=>{
        loginPage.visitLoginPage()
        cy.contains("Login to your account")
        
        cy.fixture('loginFixtures').then((invalidUser)=>{
            const loginDetails = invalidUser.invalidUserDetails[0]

            loginPage.enterMobileNumber(loginDetails.invalidNumber)
            loginPage.clickLoginButton()
            loginPage.verifyInvalidNumber()
            cy.contains("Login to your account")
        })
    })

    it("Login with empty mobile number field",()=>{
        loginPage.visitLoginPage()
        cy.contains("Login to your account")
        
            loginPage.clickLoginButton()
            loginPage.verifyInvalidNumber()
            cy.contains("Login to your account")
    })

    it("Login with incorrect OTP",()=>{
        loginPage.visitLoginPage()
        cy.contains("Login to your account")
        
        cy.fixture('loginFixtures').then((invalidUser)=>{
            const invalidLoginDetails = invalidUser.invalidUserDetails[0]
            const validLoginDetails = invalidUser.userDetails[0]

            loginPage.enterMobileNumber(validLoginDetails.anotherNumber)
            loginPage.clickLoginButton()
            loginPage.inputOtp(invalidLoginDetails.incorrectOtp)
            loginPage.clickVerifyOtp()
            loginPage.verifyInvalidOtp()
            cy.contains("Login to your account")
        })
    })

    it("Login with empty OTP field",()=>{
        loginPage.visitLoginPage()
        cy.contains("Login to your account")
        
        cy.fixture('loginFixtures').then((invalidUser)=>{
            const validLoginDetails = invalidUser.userDetails[0]
            loginPage.enterMobileNumber(validLoginDetails.anotherNumber)
            loginPage.clickLoginButton()
            loginPage.clickVerifyOtp()
            cy.wait(8000)
            loginPage.verifyEmptyOtp()
            cy.contains("Login to your account")
        })
    })

    it("Login with multiple failed OTP attempts",()=>{
        loginPage.visitLoginPage()
        cy.contains("Login to your account")
        cy.fixture('loginFixtures').then((invalidUser)=>{
            const invalidLoginDetails = invalidUser.invalidUserDetails[0]
            const validLoginDetails = invalidUser.userDetails[0]

            loginPage.enterMobileNumber(validLoginDetails.anotherNumber)
            loginPage.clickLoginButton()
            loginPage.inputOtp(invalidLoginDetails.incorrectOtp)
            loginPage.clickVerifyOtp()
            loginPage.clearOtpField()
            loginPage.inputOtp(invalidLoginDetails.incorrectOtp1)
            loginPage.clickVerifyOtp()
            loginPage.clearOtpField()
            loginPage.inputOtp(invalidLoginDetails.incorrectOtp2)
            loginPage.clickVerifyOtp()
            loginPage.clearOtpField()
            loginPage.inputOtp(invalidLoginDetails.incorrectOtp3)
            loginPage.clickVerifyOtp()
            loginPage.clearOtpField()
            loginPage.inputOtp(invalidLoginDetails.incorrectOtp4)
            loginPage.clickVerifyOtp()
            loginPage.verifyInvalidOtp()
            cy.contains("Login to your account")
        })
    })

    it("Login with more than 10 digits in mobile number field",()=>{
        loginPage.visitLoginPage()
        cy.contains("Login to your account")
        cy.fixture('loginFixtures').then((cantloginUser)=>{
        const loginDetails = cantloginUser.invalidUserDetails[0]
        loginPage.enterMobileNumber(loginDetails.moreThanTenNumber)
        loginPage.compareInputNumber(loginDetails.moreThanTenNumber)
        cy.contains("Login to your account")
        })
    })

    it("Login with mobile number less than 10 digits in the mobile number field",()=>{
    loginPage.visitLoginPage()
    cy.contains("Login to your account")
    cy.fixture('loginFixtures').then((cantloginUser)=>{
    const loginDetails = cantloginUser.invalidUserDetails[0]
    loginPage.enterMobileNumber(loginDetails.lessThanTenDigits)
    loginPage.clickLoginButton()
    loginPage.verifyInvalidNumber()
    cy.contains("Login to your account")
    })
})

    
})